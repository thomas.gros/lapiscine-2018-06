<?php

// Programme 1
/*
echo "debut du programme\n";

$n = 20;

if($n % 2 == 0) {
    echo "n est pair\n";
}

if($n % 2 != 0) {
// if($n % 2 == 1) {
    echo "n est impair\n";
}

echo "fin du programme\n";
*/

// Programme 2
/*
echo "debut du programme\n";

$n = 20;

if($n % 2 == 0) {
    echo "n est pair\n";
} else {
    echo "n est impair\n";
}

echo "fin du programme\n";
*/

/// Programme 3
///
///

// le $username est valide s'il fait 6 caractères minimum
// dans le cas ou le $username est valide afficher "username valide"
// dans le cas ou le $username est invalide, afficher "username trop petit, 6 caractères mini"

/*
$username = "thomas";

$username_has_at_least_6_chars = strlen($username) >= 6;

if($username_has_at_least_6_chars) {
    echo "username est valide";
} else {
    echo "username trop petit, 6 caractères mini ";
}
*/

/// Programme 4
///
///

// le $username est valide s'il fait 6 caractères minimum ET qu'il commence par un t
// dans le cas ou le $username est valide afficher "username valide"
// dans le cas ou le $username est invalide, afficher "username invalide"

/*
$username = "thomas";

$username_has_at_least_6_chars = strlen($username) >= 6;

$username_starts_with_a_t = $username[0] == "t";

$username_is_valid = $username_has_at_least_6_chars && $username_starts_with_a_t;

if($username_is_valid) {
    echo "username valide";
} else {
    echo "username invalide";
}
*/
/// Programme 4v2

$username = "thomas";

if(strlen($username) >= 6 && $username[0] == "t") {
    echo "username valide";
} else {
    echo "username invalide";
}

