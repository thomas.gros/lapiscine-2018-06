<?php

// la boucle for a pour objectif d'itérer

/*
$x = 3;
$x++;  // incremente x de 1
$x = $x + 1;
*/

/*
for($i = 0; $i < 100; $i++) {
    echo $i . " hello \n";
}
*/


// Programme 1
// Pour tous les nombres entre 0 et 100 (inclus) afficher si le nombre est pair ou impair
/*
for($i = 0; $i <= 100; $i++) {
    if($i % 2 == 0) {
        echo $i . " est pair\n";
    } else {
        echo $i . " est impair\n";
    }
}
*/

// Programme 2 - FizzBuzz
// a est divisible par b si a % b == 0

// Pour tous les nombres entre 0 et 100
// si le nombre est divisible par 3 alors afficher "<nombre> FIZZ"
// si le nombre est divisible par 5 alors afficher "<nombre> BUZZ"
// si le nombre est divisible par 3 ET par 5 alors afficher "<nombre> FIZZBUZZ"

/*
for($i = 0; $i <= 100; $i++) {
    if(($i % 3 == 0) && ($i % 5 == 0)) {
        echo $i . " FIZZBUZZ\n";
    } else if($i % 3 == 0) {
        echo $i . " FIZZ\n";
    } else if($i % 5 == 0) {
        echo $i . " BUZZ\n";
    }
}*/


// Programme 3 - Itération sur un tableau
$t = [1,2,3,4,5];
for ($i=0; $i < count($t); $i++) {
    echo $t[$i];
}



