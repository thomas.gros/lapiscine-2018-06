<?php

// Programme 1
echo "debut du programme\n";

$temp = 30;

if($temp < 15) {
    echo "mettre le chauffage\n";
} else if ($temp >= 15 && $temp < 25) {
    echo "ouvrir un peu la fenêtre\n";
} else if ($temp >= 25 && $temp < 30) {
    echo "ouvrir en grand la fenêtre\n";
} else {
    echo "mettre la clim\n";
}

echo "fin du programme\n";

// Programme 2
$note = 12;

// si note < 10 => afficher "bof bof..."
// si 10 <= note < 12 => afficher "passable"
// si 12 <= note < 14 => afficher "assez bien"
// si 14 <= note < 16 => afficher "bien"
// si note >= 16 => afficher "très bien"

$bofbof = $note < 10;
$passable = $note >= 10 && $note < 12;
$assezbien = $note >= 12 && $note < 14;
$bien = $note >= 14 && $note < 16;

if($bofbof) {
    echo "bofbof";
} else if ($passable) {
    echo "passable";
} else if ($assezbien) {
    echo "assez bien";
} else if ($bien) {
    echo "bien";
}  else {
    echo "très bien";
}

// Programme 2v2, plus "optimal" car les calculs ne sont pas tous nécessairement faits.
if($note < 10) {
    echo "bofbof";
} else if ($note >= 10 && $note < 12) {
    echo "passable";
} else if ($note >= 12 && $note < 14) {
    echo "assez bien";
} else if ($note >= 14 && $note < 16) {
    echo "bien";
}  else {
    echo "très bien";
}

// Programme 2v3 // encore un peu plus "optimal" car moins de "tests"
if($note < 10) {
    echo "bofbof";
} else if ($note < 12) {
    echo "passable";
} else if ($note < 14) {
    echo "assez bien";
} else if ($note < 16) {
    echo "bien";
}  else {
    echo "très bien";
}







