<?php

$notes = [12, 20, 6, 13, 14];

// Ecrire un programme qui affiche true si au moins une note = 20 et false sinon

$trouve_20 = false;

for($i = 0; $i < count($notes); $i++) {
    if($notes[$i] == 20) {
        $trouve_20 = true;
    }
}

if($trouve_20) {
    echo "true";
} else {
    echo "false";
}