<?php

// if démarque une branche de code dans laquelle on va 'rentrer', cad
// qui va s'exécuter SI (if) une condition est true

/* // Programme 1
echo "debut du programme\n";

if(true) { // block d'instructions délimités par des accolades
    echo "hello\n";
    echo "le if\n";
}

echo "fin du programme\n";
*/

/* // Programme 2
echo "debut du programme\n";

$n = 10;

$n_est_pair = ($n % 2) == 0;

 if($n_est_pair) {
// if(($n % 2) == 0) {
    echo "n est pair\n";
}

echo "fin du programme\n";
*/

echo "debut du programme\n";

$a = 10;
$b = 20;

if(($a > 5) and ($b < 30)) {
    echo "a est plus grand que 5 et b est plus petit que 30";
}

if(($a > 5) && ($b < 30)) { // ne vérifie pas l'opérande de droite si celle de gauche est false
    echo "a est plus grand que 5 et b est plus petit que 30";
}

if(!($a <= 5) and !($b >= 30)) {
    echo "a est plus grand que 5 et b est plus petit que 30";
}

if(! (($a <= 5) or ($b >= 30))) {
    echo "a est plus grand que 5 et b est plus petit que 30";
}

if(! (($a <= 5) || ($b >= 30))) { // ne vérifie pas l'opérande de droite si celle de gauche est true
    echo "a est plus grand que 5 et b est plus petit que 30";
}

if($a > 5) {
    if($b < 30) {
        echo "a est plus grand que 5 et b est plus petit que 30";
    }
}






echo "fin du programme\n";
