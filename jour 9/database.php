<?php

require_once 'Photo.php';

/**
 * Retourne toutes les photos présentes dans la base de données
 * @return $photos[] un tableau contenant toutes les photos présentes dans la base de données
 */
function find_all_photos() {
    $photos = [];

// dans la vraie vie, ces paramètres sont à définir à l'extérieur du code source
// par exemple dans des variables d'environnement
    $database = "mysql:host=localhost;port=5000;dbname=lapiscine_photo";
    $username = "root";
    $password = "root";

    $conn = new PDO($database, $username, $password);

    $query = "SELECT id, path, title, published_on FROM photo";

    $result = $conn->query($query);

    foreach ($result as $row) {
        // $photos[] = $row;
        $photos[] = new Photo($row['id'], $row['path'], $row['title'], $row['published_on']);
    }

    $conn = null;

    return $photos;
}