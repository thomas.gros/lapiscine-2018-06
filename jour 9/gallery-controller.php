<?php

require_once 'Photo.php';

// require_once 'database.php';
require_once 'PhotoRepository.php';

$repo = new PhotoRepository();

$photos = $repo->findAllPhotos();

require_once 'gallery-view.php';