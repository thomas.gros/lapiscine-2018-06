#TP
Dans ce TP vous allez réaliser une gallerie photo, comme ici

![photo flickr](https://librisblog.photoshelter.com/wp-content/uploads/2015/07/Explore___Flickr_-_Photo_Sharing_.png")

## Les données

Les données vont provenir d'une base de données.

1. Créer une base de données qui s'appelle `lapiscine_photo`.
2. Créer une table `$photo` qui a les colonnes suivantes:
  * `id` : `int`, `primary key`, `auto increment`
  * `path`: `varchar(255)`, `not null`, `unique`  (correspond à l'emplacement du fichier de la photo sur le disque dur)
  * `title`: `varchar(255)`, `not null`
  * `published_on` `datetime`, `not null`
3. Insérer des données dans la table `lapiscine_photo.photo`.

## Model (database.php)

1. Créer un fichier `database.php`.
2. Dans `database.php` créer une fonction qui s'appelle `find_all_photos` et qui retourne un tableau
contenant un élément par photo présente dans la table `lapiscine_photo.photo`.
Cet élément est un tableau.
3. Vérifier (par exemple via un `var_dump`) via un petit script `database-test.php` que la fonction `find_all_photos`
fonctionne bien.

## Passage en objet

Au lieu de retourner des tableaux correspondant à des photos on va retourner des objets correspondants à des photos.
1. Créer une classe `Photo` ayant comme attributs: `id`, `path`, `titre`, `publishedOn`
2. Créer les getters et setters correspondants.
3. Créer un constructeur adapté.
4. Modifier la fonction `find_all_photos` de `database.php` pour qu'elle retourne un tableau de photos.
C'est à dire dans votre boucle il faut transformer chaque `$row` en `new Photo(...)`.
5. Exécuter `database-test.php` pour vérifier que tout marche toujours bien

## Création d'une application web gallerie photo

En vous inspirant de article-list-controller.php et article-list-view.php
1. Ecrire gallery-controller.php
2. Ecrire gallery-view.php

## Un peu plus d'objet

Le fichier `database.php` fonctionne mais n'est pas très 'objet'. Nous allons le remplacer.

1. Créer un fichier qui s'appelle `PhotoRepository.php`
2. Créer dans ce fichier une classe `PhotoRepository`
3. Dans cette classe créer une méthode `findAllPhotos()`
4. Copier // coller le code de `database.php` dans la méthode `findAllPhotos()`
  * ne pas oublier le return...
5. Modifier gallery-controller.php.
  * `require 'PhotoRepository.php'`
  * `$repo = new PhotoRepository()`
  * `$photos = $repo->findAllPhotos()`
  
## Nouvelle fonctionnalité: détail d'une photo

Il faut maintenant rajouter une nouvelle fonctionnalité: quand on clique sur 
une photo de la gallerie, on doit arriver sur une page dédiée à cette photo.

1. Créer une méthode `findPhotoById($id)` dans la classe `PhotoRepository`
2. En s'inspirant de l'exercice sur les articles, créer les fichiers
  ** gallery-photo-detail-controller.php
  ** gallery-photo-detail-view.php