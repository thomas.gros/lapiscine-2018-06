<?php
/**
 * Created by PhpStorm.
 * User: thomasgros
 * Date: 27/06/2018
 * Time: 16:20
 */

class Photo
{

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $path;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $publishedOn;

    /**
     * Photo constructor.
     * @param int $id
     * @param string $title
     * @param string $path
     * @param string $publishedOn
     */
    public function __construct(int $id, string $path, string $title, string $publishedOn)
    {
        $this->id = $id;
        $this->path = $path;
        $this->title = $title;
        $this->publishedOn = $publishedOn;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @param string $path
     */
    public function setPath(string $path): void
    {
        $this->path = $path;
    }

    /**
     * @return string
     */
    public function getPublishedOn(): string
    {
        return $this->publishedOn;
    }

    /**
     * @param string $publishedOn
     */
    public function setPublishedOn(string $publishedOn): void
    {
        $this->publishedOn = $publishedOn;
    }



}