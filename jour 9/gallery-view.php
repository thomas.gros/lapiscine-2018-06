<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Tous les photos</title>
</head>
<body>

<section>
<?php foreach($photos as $photo) { ?>
    <div>
        <a href="http://piscine.loc/jour%209/gallery-photo-detail-controller.php?id=<?= $photo->getId() ?>">
            <img src="<?= $photo->getPath() ?>" />
            <p><?= $photo->getTitle() ?></p>
        </a>
    </div>
<?php } ?>

</section>

</body>
</html>