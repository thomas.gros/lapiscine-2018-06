<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?= $article['title'] ?></title>
</head>
<body>

<a href="http://piscine.loc/jour%207/article-list-controller.php">Tous les articles</a>
<section>
    <article>
        <h2><?= $article['title'] ?></h2>
        <p><?= $article['date'] ?></p>
        <p><?= $article['content'] ?></p>
    </article>
</section>

</body>
</html>