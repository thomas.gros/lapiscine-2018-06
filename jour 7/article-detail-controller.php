<?php

require_once 'database.php';

function response_404()
{
    http_response_code(404);
    require_once 'my-404-view.php';
}

function is_id_invalid()
{
    return !array_key_exists('id', $_GET)
        || !ctype_digit($_GET['id']);
}


if (is_id_invalid()) {
    response_404();
} else {

    $article = find_article_by_id($_GET['id']);

    if ($article == NULL) {
        response_404();
    } else {
        require_once 'article-detail-view.php';
    }
}
