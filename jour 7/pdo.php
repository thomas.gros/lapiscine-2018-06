<?php

// dans la vraie vie, ces paramètres sont à définir à l'extérieur du code source
// par exemple dans des variables d'environnement
$database = "mysql:host=localhost;port=5000;dbname=lapiscine_blog";
$username = "root";
$password = "root";

$conn = new PDO($database, $username, $password);

$query = "SELECT id, title, date, content FROM article";

$result = $conn->query($query);

foreach ($result as $row) {
    var_dump($row[1]);
}

$conn = null;
