<?php

// Exemples de fonctions

// strnlen
// - 'recoit' une information | paramètre | argument ici "coucou"
// - retourne la longueur en bytes de "coucou"
$len = strlen("coucou");

// var_dump
// - recoit une information, ici 42
// - écrit dans la sortie standard des informations sur la valeur 42
var_dump(42);

// count
// - recoit une information, ici [1,2,3]
// - retourne le nombre d'éléments de [1,2,3]
echo count([1,2,3]);

// Création de fonctions
function addition($a, $b) {
    $res = $a + $b;
    return $res;
}

echo addition(2,5);
$add = addition(12,30);
echo $add;


function multiplication($a, $b) {
    $res = $a * $b;
    return $res;
}

echo multiplication(4,10);

function concat($a, $b){
    $res = $a . $b;
    return $res;
}

concat("bonjour", " le monde"); // => "bonjour le monde";


function additionv2($tableau) {
    $res = 0;
    foreach($tableau as $v) {
        $res = $res + $v;
    }

    return $res;
}

$res = additionv2([1,2,3,4,5]);
var_dump($res);


function moyenne($tableau) {
    $sum = 0;
    foreach($tableau as $v) {
        $sum = $sum + $v;
    }

    return $sum / count($tableau);
}

$res = moyenne([1,2,3,4,5]);
var_dump($res); // 3

function moyennev2($tableau) {
    return additionv2($tableau) / count($tableau);
}

$res = moyennev2([1,2,3,4,5]);
var_dump($res); // 3


// Ecrire une fonction findmin qui
// - prend en paramètre un tableau
// - retourne la plus petite valeur présente dans ce tableau

function findmin($tableau) {
    $min = $tableau[0];

    for($i = 1; $i < count($tableau); $i++) {
        if($tableau[$i] < $min) {
            $min = $tableau[$i];
        }
    }

    return $min;
}

echo findmin([1,2,3]);
echo findmin([5,4,6]);


function ne_retourne_rien($n) {
    for($i = 0; $i < $n; $i++) {
        echo "hello " .$i;
    }
}

ne_retourne_rien(3);
ne_retourne_rien(10);

var_dump(ne_retourne_rien(1));


function dessine_moi_une_ligne($n) {
    for($i = 0; $i < $n; $i++) {
        echo "#";
    }
    echo "<br>";
}

function dessine_moi_un_carre($taille_du_carre) {
    //
    for($i = 0; $i < $taille_du_carre; $i++) {
        dessine_moi_une_ligne($taille_du_carre);
    }

}

function dessine_moi_un_carrev2($taille_du_carre) {
    //
    for($i = 0; $i < $taille_du_carre; $i++) {
        for($j = 0; $j < $taille_du_carre; $j++) {
            echo "#";
        }
        echo "<br>";
    }
}


dessine_moi_un_carrev2(2);
##
##

dessine_moi_un_carrev2(4);
####
####
####
####

// A faire pour exercices personnels
//dessine_moi_un_triangle(4);
//####
//###
//##
//#
//
//dessine_moi_un_double_triangle(4)
//#
//##
//###
//####
//###
//##
//#