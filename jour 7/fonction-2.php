<?php

// $a est un paramètre.
// Le scope (portée) de $a est local à la fonction.
// le paramètre $a va 'masquer' les variables de scope plus 'global'
function my_function($a) {
    echo $a;
    return $a * $a;
}

$a = 4;


my_function($a);
my_function(10);
