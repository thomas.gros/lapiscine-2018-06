<?php

/**
 * Retourne tous les articles présents dans la base de données
 * @return $articles[] un tableau contenant tous les articles présents dans la base de données
 */
function find_all_articles() {
    $articles = [];

// dans la vraie vie, ces paramètres sont à définir à l'extérieur du code source
// par exemple dans des variables d'environnement
    $database = "mysql:host=localhost;port=5000;dbname=lapiscine_blog";
    $username = "root";
    $password = "root";

    $conn = new PDO($database, $username, $password);

    $query = "SELECT id, title, date, content FROM article";

    $result = $conn->query($query);

    foreach ($result as $row) {
        $articles[] = $row;
    }

    $conn = null;

    return $articles;
}


/**
* Retourne un article présent dans la base de données.
* @param id l'identifiant de l'article à récupérer dans la base de données
* @return $article un article si il existe dans la base un article avec cet id. null sinon.
*/
function find_article_by_id($id) {
    $article = null;

// dans la vraie vie, ces paramètres sont à définir à l'extérieur du code source
// par exemple dans des variables d'environnement
    $database = "mysql:host=localhost;port=5000;dbname=lapiscine_blog";
    $username = "root";
    $password = "root";

    $conn = new PDO($database, $username, $password);

    // attention aux INJECTIONS SQL !!!
    // https://www.owasp.org/images/7/72/OWASP_Top_10-2017_%28en%29.pdf.pdf
    // https://www.owasp.org/index.php/Main_Page
    // $query = "SELECT id, title, date, content FROM article WHERE id = ".$id;
    // $result = $conn->query($query);

    // http://php.net/manual/en/pdo.prepared-statements.php
    $query = "SELECT id, title, date, content FROM article WHERE id = :id"; // créer une requête 'paramétrée'
    $stmt = $conn->prepare($query); // obtenir un 'PreparedStatement'
    $stmt->bindParam(':id', $id); // associer les paramètres à leurs valeurs
    $stmt->execute(); // exécuter la requête sur le serveur mysql

    $row = $stmt->fetch(); // fetch pour récupérer une ligne ou false si résultat vide
    if($row != false) {
        $article = $row;
    }

    $conn = null;

    return $article;
}