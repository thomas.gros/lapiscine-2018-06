<?php

$v = 42;
echo gettype($v); // integer

$v2 = "hello php";
echo gettype($v2); // string

// typage 'dynamique'
$v = "hello ?"; // affecte une nouvelle valeur à $v. Maintenant $v contient "hello ?" et 42 est 'perdu'.
echo gettype($v); // string

$v = true;
echo gettype($v); // boolean

/************
 * BOOLEANS
 ************/
$b1 = true;
$b2 = false;

echo $b1; // affiche 1
echo $b2; // affiche heu... rien
var_dump($b1);
var_dump($b2);

// opérateurs booléens - logiques http://be2.php.net/manual/en/language.operators.logical.php
echo "operateurs logiques \n";
// var_dump(3 + 5);
echo "operateurs logiques - and \n";
var_dump( true and true); // true and true => true
var_dump( true and false);  // true and false => false
var_dump( false and true); // false and true => false
var_dump( false and false); // false and false => false

echo "operateurs logiques - or \n";
var_dump( true or true); // true or true => true
var_dump( true or false);  // true or false => true
var_dump( false or true); // false or true => true
var_dump( false or false); // false or false => false

echo "operateurs logiques - xor \n";
var_dump( true xor true); // true xor true => false
var_dump( true xor false);  // true xor false => true
var_dump( false xor true); // false xor true => true
var_dump( false xor false); // false xor false => false

echo "operateurs logiques - ! qui veut dire not \n";
var_dump( ! true); // ! true => false
var_dump( ! false); // ! false => true


echo "entrainement \n";
var_dump( (true and true ) or false); // true
var_dump( true and (true or false)); // true

var_dump( ( (! true) xor true) or false ); // true

echo "operateurs de comparaison \n"; // http://be2.php.net/manual/en/language.operators.comparison.php

var_dump( 3 == 4); // égalité => false
var_dump( 4 == 4); // égalité => true

var_dump( 3 != 4); // différent => true
var_dump( 4 != 4); // différent => false

var_dump( ! (3 == 4)); // equivalent à la différence, => true

var_dump( 3 >= 4); // supérieur ou égal => false
var_dump( 3 <= 4); // supérieur ou égal => true

var_dump( 3 > 4); // strictement supérieur => false
var_dump( 3 < 4); // strictement inférieur => true

var_dump( 4 >= 4); // true
var_dump( 4 > 4); // false



/************
 * Nombres
 ************/

// integer
$i1 = 42; // integer
var_dump(gettype($i1));
$i2 = -123;
$i3 = 0123; // 0 représente la base octale
var_dump($i3);
$i4 = 0x123; // 0x représente la base hexadécimale
var_dump($i4);
$i5 = 0b100; // représente la base binaire
$i5 = 4;
var_dump($i5);

echo PHP_INT_SIZE; // 8 = 64 bits , 4 = 32 bits
echo PHP_INT_MAX; // plus grand entier possible en PHP
echo PHP_INT_MIN; // plus petit entier possible en PHP
// 1111111111111111111111111


// floating point number (nombre à virgule flottante)
// LIRE http://floating-point-gui.de/, cf aussi http://be2.php.net/manual/en/language.types.float.php
$f1 = 0.42;
var_dump($f1);

var_dump(0.1 + 0.2); // 0.3 mais en interne il se passe des trucs...
var_dump(1/3); // 0.333333333

echo floor(0.1); // 0
echo floor(1.5); // 1

echo floor( (0.1+0.7) * 10); // 7 mais devrait être 8

var_dump(0.1 + 0.2 == 0.3); // ATTENTION DANGER
$f2 = 0.1 + 0.2;
$f3 = 0.3;
$delta = 0.0000001;
var_dump( $f2 - $f3 < $delta);


/************
 * String - Chaines de caractères
 ************/

$s1 = "ceci est une chaine de caractère";
$s2 = 'ceci est une autre chaine de caractères';
var_dump($s1);


// encodage historique ASCII - 8bits

// Aujourd'hui la plupart du temps on encore en UNICODE => UTF-8, UTF-16,...
// https://www.joelonsoftware.com/2003/10/08/the-absolute-minimum-every-software-developer-absolutely-positively-must-know-about-unicode-and-character-sets-no-excuses/
// http://kunststube.net/encoding/
echo $s1;
echo "😃";
$s3 = "\u{1F30A} \u{1F30F} \u{1F600}";
echo $s3;


$s4 = "bonjour";
$s5 = "le monde";

$s6 = $s4 . $s5; // concaténation => assembler 2 string en 1;. $s4 et $s5 ne sont pas modifiées.
echo $s6;

echo $s4 . " " . $s5;

echo "affichage par indice \n";

echo $s4[0]; // b        crochets = alt+shift+( ou alt+shift+)
echo $s4[1]; // o
echo $s4[2]; // n
var_dump($s4[7]); // php ne rale pas mais bon... éviter d'accéder à un indice en dehors de la string

echo strlen($s4); //strlen = retourne le nombre de caractères dans une string

$s4[0] = "_";
var_dump($s4);

/************
 * Array - tableaux
 ************/
$t = []; // tableau vide
var_dump($t);

$t2 = [1,2,3,4,5]; // tableau de 5 éléments: 1,2,3,4 et 5
var_dump($t2);

$t3 = ["bonjour", "le", "monde"];
var_dump($t3);

$t4 = [1, true, "bonjour", 3.42];
var_dump($t4);

var_dump($t3[0]);
var_dump($t3[1]);
var_dump($t3[2]);

echo $t3[0] . " " . $t3[1] . " " . $t3[2];

echo count($t3); // count compte le nombre d'éléments d'un tableau

// $t3 = ["bonjour", "le", "monde"];
$t3[0] = "hello";
$t3[2] = "world";
var_dump($t3);













