<?php

    // les opérateurs permettent de faire des opérations sur des opérandes.
    // + - / * % = == === | || & && < > <= => ...

    // opérations => produisent un résultat

    // assembler plusieurs opérations ensemble dans une expression

    // 1 + 1
    // 1 - 1
    // 5 / 2

//echo 1 + 1;
//var_dump(1 + 1);
//
//var_dump( 5 / 2);
//
//var_dump( 5 + 2) / 3;
//var_dump(7) / 3;
//NULL / 3;   // En PHP NULL <=> 0
//0 / 3;
//
//var_dump(5 + 2) / 3;

// précédence des opérateurs
// echo 5 % 2 >> 3;
// echo (5 + 2) * 3;

// bit shift : on shift (décale) 2 de 3 positions vers la gauche
// bit: 0 ou 1

// en base 10
236 = 200     + 30     + 6
    = 2 * 100 + 3 * 10 + 6
    = 2 * 10^2 + 3 * 10^1 + 6 * 10^0


1024 = 4 * 10^0
    +  2 * 10^1
    +  0 * 10^2
    +  1 * 10^3


// en base 10
1001001001 = 1 * 10^0
           + 0 * 10^1
           + 0 * 10^2
           + 1 * 10^3
           ...

// en base 2
1001001001 = 1 * 2^0
           + 0 * 2^1
           + 0 * 2^2
           + 1 * 2^3
                  ...
// en base 8
1001001001 = 1 * 8^0
           + 0 * 8^1
           + 0 * 8^2
           + 1 * 8^3
                  ...




// base 2
0 = 0 * 2^0 = 0 * 1 = 0
1 = 1 * 2^0 = 1 * 1 = 1
10 = 1 * 2^1 + 0 * 2^0 = 1 * 2 + 0 = 2
11 = 1 * 2^1 + 1*2^0 = 1 * 2 + 1 * 1 = 3
100 = 4
101 = 5
110 = 6
111 = 7



// en base hexadecimale -> 16 valeur par chiffre (base 16)
// 0..F
//
//F = 15 * 16^0
//
//A = 10 * 16^0
//
//9 = 9 * 16^0
//
//10 = 1 * 16^1 + 0*16^0

//AB =  10 * 16^1 + 11* 16^0   = 171


//0F = 0 * 16^1 + 15 * 16^0
//1F = 1 * 16^1 + 15 * 16^0



64 => 6 * 16^1 + 4 * 16^0 = 6 * 16 + 4 * 1 = 96 + 4 = 100
101 => 1 * 2^2 + 0 * 2^1 + 1 * 2^0 = 4 + 0 + 1 = 5





// bit: 0/1
// byte = octet = 8-bit 11111111 <=> 00000000












