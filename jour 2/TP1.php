<?php

/**
 * Soit une chaine de caractère $s.
 * Ecrire un programme qui affiche (var_dump)
 * true si la longueur de la chaine de caractères est plus petite que 10,
 * false sinon
 */

echo "\nExercice Opérateur Logique\n";

// V1
$s = "bonjour";

$length = strlen($s);

$est_plus_petit_que_10 = $length < 10;

var_dump($est_plus_petit_que_10);


// V2
$s = "bonjour";

$est_plus_petit_que_10 = strlen($s) < 10;

var_dump($est_plus_petit_que_10);


// V3

$s = "bonjour";
var_dump(strlen($s) < 10);

// V4
var_dump(strlen("bonjour") < 10);



// a % b = reste de la division entière de a par b.
// a = b * q + r     <= Le modulo est r
// 10 % 3 = 1
// 10 = 3 * 3 + 1

// 10 % 2
// 10 = 2 * 5 + 0 // nombre pair => % 2 = 0

// 7 % 2
// 7 = 2 * 3 + 1  // nombre impair => %2 = 1
/**
 * Soit une variable $n.
 * Ecrire un programme qui affiche (var_dump) true si la variable est pair, false sinon
 * Une variable est pair si son modulo 2 (par exemple $n % 2) vaut 0
 */
echo "\nExercice modulo\n";

$n = 42;

$n_modulo_2 = $n % 2; // 0 ou 1

$n_est_pair = $n_modulo_2 == 0;
// $n_est_pair = $n_modulo_2 != 1;

var_dump($n_est_pair);


/**
 * Soit un tableau de 5 notes $t = [10, 12, 8, 14, 17];
 * Ecrire un programme:
 * qui affiche le total de ces 5 notes
 * qui affiche la moyenne de ces 5 notes
 */

echo "\nExercice notes\n";

$t = [8, 12, 8, 14, 17];

$sum = $t[0] + $t[1] + $t[2] + $t[3] + $t[4];

echo $sum;
echo "\n";
echo $sum / 5;   // on peut remplacer 6 par count($t)