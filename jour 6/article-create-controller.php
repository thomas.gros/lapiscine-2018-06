<?php

$errors = [];

if($_SERVER['REQUEST_METHOD'] === 'POST') {

    // vérification des paramètres requis et optionnels du formulaire soumis dans la requête

    // title
    if( ! array_key_exists('title', $_POST)
        || mb_strlen($_POST['title']) < 8) {
        $errors["title"] = "le titre doit faire à minimum 8 caractères";
    }

    //date
    if( ! array_key_exists('date', $_POST)
        || mb_strlen($_POST['date']) < 1) {
        $errors["date"] = "la date doit être spécifiée";
    }

    //content
    if( ! array_key_exists('content', $_POST)
        || mb_strlen($_POST['content']) < 1) {
        $errors["content"] = "le contenu doit être spécifié";
    }

    if(empty($errors)) {
        // si les paramètres sont valides => faire les traitements appropriés
        // TODO enregistrer l'article dans la base de données

        // demander au navigateur de faire une redirection http sur l'url 'http://...article-list-controller.php'
        header("Location: http://piscine.loc/jour%206/article-list-controller.php");
    } else {
        require_once 'article-create-view.php';
    }

} else {
    require_once 'article-create-view.php';
}