<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Creation d'article</title>
</head>
<body>

<form action="" method="post">
    <div>
        <label for="title">Title</label>
        <input id="title" type="text" name="title" value="<?= $_POST['title'] ?? '' ?>">
        <span><?= $errors["title"] ?? '' // ?? est le null coalescing operator, PHP 7 uniquement ?></span>
    </div>
    <div>
        <label for="date">Date</label>
        <input id="date" type="date" name="date" value="<?= $_POST['date'] ?? '' ?>">
        <span><?= $errors["date"] ?? '' ?></span>
    </div>
    <div>
        <label for="content">Contenu</label>
        <textarea id="content" name="content"><?= $_POST['content'] ?? '' ?></textarea>
        <?php if (array_key_exists("content", $errors)) {?>
            <span><?= $errors["content"] ?></span>
        <?php } ?>
    </div>
    <input type="submit">
</form>

</body>
</html>