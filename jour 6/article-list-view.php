<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Tous les articles</title>
</head>
<body>

<section>

<?php foreach($articles as $article) { ?>
    <article>
        <a href="http://piscine.loc/jour%206/article-detail-controller.php?id=<?= $article['id'] ?>"><h2><?= $article['title'] ?></h2></a>
        <p><?= $article['date'] ?></p>
        <p><?= $article['content'] ?></p>
    </article>
<?php } ?>

</section>

</body>
</html>