<?php

require_once 'database.php';

if(    ! array_key_exists('id', $_GET) // si il n'y a pas de clé 'id' dans $_GET => 404
    || ! ctype_digit($_GET['id'])) {  // si $_GET['id'] n'est pas composée uniquement de digit

    http_response_code(404);
    require_once 'my-404-view.php';
} else {

    $id = $_GET['id'];
    settype($id, 'int'); // $id = (int) $id;

    $article = NULL; // on cherche un article d'id $id. On considère au début que l'article n'est pas trouvé.

    foreach ($articles as $a) {
        if($id === $a["id"]) { // TROUVE
            $article = $a;
        }
    }

    if($article == NULL) { // si aucun article trouvé => 404
        http_response_code(404);
        require_once 'my-404-view.php';
    } else {
        require_once 'article-detail-view.php';
    }
}



