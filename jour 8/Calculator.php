<?php

class Calculator
{

    private $history;

    /**
     * Calculator constructor.
     */
    public function __construct()
    {
        $this->history = [];
    }

    public function add($a, $b)
    {
        $this->history[] = "add " . $a . ",".$b;
        return $a + $b;
    }

    public function mult($a, $b)
    {
        $this->history[] = "mult " . $a . ",".$b;
        return $a * $b;
    }

    /**
     * @return array
     */
    public function getHistory(): array
    {
        return $this->history;
    }
}