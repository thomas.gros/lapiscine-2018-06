<?php

require_once 'Rectangle.php';

function calcarea($rect) {
    return $rect->width * $rect->height;
}

$r1 = new Rectangle();
$r1->width = 10;
$r1->height = 10;
var_dump($r1);
var_dump(calcarea($r1));
var_dump($r1->calcarea());


$r2 = new Rectangle();
$r2->width = 100;
$r2->height = 200;
var_dump($r2);
var_dump(calcarea($r2));
var_dump($r2->calcarea());

$r3 = new Rectangle();
var_dump($r3);