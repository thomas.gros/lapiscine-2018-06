<?php

/**
 */
class Person
{

    /**
     * @var null|string
     */
    private $firstname;

    /**
     * @var null|string
     */
    private $lastname;

    /**
     * Person constructor.
     * @param null|string $firstname
     * @param null|string $lastname
     */
    public function __construct(?string $firstname = null, ?string $lastname = null)
    {
        $this->firstname = $firstname;
        $this->lastname = $lastname;
    }

    /**
     * @return null|string
     */
    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    /**
     * @param null|string $firstname
     */
    public function setFirstname(?string $firstname): void
    {
        $this->firstname = $firstname;
    }

    /**
     * @return null|string
     */
    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    /**
     * @param null|string $lastname
     */
    public function setLastname(?string $lastname): void
    {
        $this->lastname = $lastname;
    }


    public function fullname()
    {
        return $this->firstname . " " . $this->lastname;
    }


}