<?php

require_once 'Person.php';

$p1 = new Person("thomas", "gros");
$p2 = new Person("john", "doe");

var_dump($p1->getFirstname());

$p1->setFirstname("le_nouveau_firstname");

var_dump($p1->getFirstname());

//$p1->firstname = 'thomas';
//$p1->lastname = 'gros';

var_dump($p1->fullname());