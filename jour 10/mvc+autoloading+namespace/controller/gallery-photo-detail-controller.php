<?php

require_once '..'.DIRECTORY_SEPARATOR.'bootstrap.php';

function response_404()
{
    http_response_code(404);
    require_once '../view/my-404-view.php';
}

function is_id_invalid()
{
    return !array_key_exists('id', $_GET)
        || !ctype_digit($_GET['id']);
}


if (is_id_invalid()) {
    response_404();
} else {

    $repo = new model\repository\PhotoRepository();
    $photo = $repo->findPhotoById($_GET['id']);

    if ($photo == NULL) {
        response_404();
    } else {
        require_once '../view/gallery-photo-detail-view.php';
    }
}
