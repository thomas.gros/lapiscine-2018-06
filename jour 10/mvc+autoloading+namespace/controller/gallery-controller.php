<?php


require_once '..'.DIRECTORY_SEPARATOR.'bootstrap.php';

$repo = new model\repository\PhotoRepository();

$photos = $repo->findAllPhotos();

require_once '../view/gallery-view.php';