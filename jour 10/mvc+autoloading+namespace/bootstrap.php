<?php

spl_autoload_register(function($classname) {
    $BASE_PATH = "/Users/thomasgros/PhpstormProjects/PhpBasic/jour 10/mvc+autoloading+namespace";

    $classname_with_antislash = str_replace("\\","/", $classname);

    require_once $BASE_PATH.DIRECTORY_SEPARATOR.$classname_with_antislash.'.php';

});