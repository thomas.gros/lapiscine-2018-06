<?php

// enregistrer les classes pour qu'elles soient utilisables directement plus tard.
// ce processus s'appelle l'autoloading.
// http://php.net/manual/en/language.oop5.autoload.php

spl_autoload_register(function($classname) {
    var_dump($classname);


    require_once $classname.'.php';

//    if($classname === 'B') {
//        require_once 'B.php';
//    } else if($classname === 'A') {
//        require_once 'A.php';
//    }

});