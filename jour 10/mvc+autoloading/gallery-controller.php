<?php

require_once 'bootstrap.php';

$repo = new PhotoRepository();

$photos = $repo->findAllPhotos();

require_once 'gallery-view.php';