<?php

class PhotoRepository
{

    function findAllPhotos()
    {
        $photos = [];

// dans la vraie vie, ces paramètres sont à définir à l'extérieur du code source
// par exemple dans des variables d'environnement
        $database = "mysql:host=localhost;port=5000;dbname=lapiscine_photo";
        $username = "root";
        $password = "root";

        $conn = new PDO($database, $username, $password);

        $query = "SELECT id, path, title, published_on FROM photo";

        $result = $conn->query($query);

        foreach ($result as $row) {
            // $photos[] = $row;
            $photos[] = new Photo($row['id'], $row['path'], $row['title'], $row['published_on']);
        }

        $conn = null;

        return $photos;
    }

    public function findPhotoById($id)
    {
        $photo = null;

        $database = "mysql:host=localhost;port=5000;dbname=lapiscine_photo";
        $username = "root";
        $password = "root";

        $conn = new PDO($database, $username, $password);

        $query = "SELECT id, path, title, published_on FROM photo WHERE id = :id";
        $stmt = $conn->prepare($query);
        $stmt->bindParam(':id', $id);
        $stmt->execute();

        $row = $stmt->fetch();
        if($row != false) {
            $photo = new Photo($row['id'], $row['path'], $row['title'], $row['published_on']);
        }

        $conn = null;

        return $photo;
    }
}