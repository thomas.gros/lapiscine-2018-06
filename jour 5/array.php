<?php

echo "Les tableaux";

// les éléments sont indicés numériquement, automatiquement de 0 à count($t) - 1
$t = [1,2,3,4,5];

var_dump($t);


// les éléments sont indicés numériquement, 'à la main'
$t2 = [
        0 => 1,
        1 => 2,
        2 => 3,
        3 => 4,
        4 => 5
];

var_dump($t2);

// les indices ne sont pas nécessairement consécutifs
$t3 = [
    10 => 1,
    11 => 2,
    20 => 3,
    30 => 4,
    40 => 5
];

var_dump($t3);
var_dump($t3[20]); // 3
var_dump(count($t3));

// en pratique en PHP un tableau est un ensemble de couples key => value
$t4 = [
    "nom" => "gros",
    "prenom" => "thomas"
];

var_dump($t4);
var_dump(count($t4));
var_dump($t4["prenom"]);


// parcourir un tableau
// si les éléments sont indicés (key) séquentiellement
for($i = 0; $i < count($t); $i++) {
    echo $t[$i];
    echo "<br>";
}

// parcourir dans l'ordre les éléments, quelque soient les keys
foreach ($t4 as $key => $value) {
    echo $key . " " . $value;
    echo "<br>";
}

// ici on itère uniquement sur les value si pas besoin de la key
foreach ($t3 as $value) {
    echo $value;
    echo "<br>";
}

