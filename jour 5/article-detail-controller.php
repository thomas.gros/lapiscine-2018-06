<?php

var_dump($_GET['id']);

// var_dump($_SERVER);
var_dump($_GET);

// query string
//?toto=tata&titi=2&tutu=bonjour




///$id = ???
//var_dump($_GET);


$article = [
        "title" => "PHP 5.6.34 Released",
        "date" => "01 Mar 2018",
        "content"=> "The PHP development team announces the immediate availability of PHP 5.6.34. This is a security release. One security bug was fixed in this release. All PHP 5.6 users are encouraged to upgrade to this version. For source downloads of PHP 5.6.34 please visit our downloads page, Windows source and binaries can be found on windows.php.net/download/. The list of changes is recorded in the ChangeLog."
    ];

include 'article-detail-view.php';