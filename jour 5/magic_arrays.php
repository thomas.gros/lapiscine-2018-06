<?php

// Dans PHP il existe un ensemble de variables "pré-définies"
// http://be2.php.net/manual/en/reserved.variables.php

// certaines sont "super-globales" http://be2.php.net/manual/en/language.variables.superglobals.php

// var_dump($GLOBALS);
// var_dump($_SERVER);
// var_dump($_REQUEST);
// var_dump($_GET);

// générer une table HTML, 2 colonnes. key => value
// pour les clés: SERVER_NAME, SERVER_PORT, REQUEST_URI


echo "<table border=1>";
    echo "<tr>";
        echo "<td>SERVER_NAME</td>";
        echo "<td>".$_SERVER['SERVER_NAME']."</td>";
    echo "</tr>";
    echo "<tr>";
        echo "<td>SERVER_PORT</td>";
        echo "<td>".$_SERVER['SERVER_PORT']."</td>";
    echo "</tr>";
    echo "<tr>";
        echo "<td>REQUEST_URI</td>";
        echo "<td>".$_SERVER['REQUEST_URI']."</td>";
    echo "</tr>";
echo "</table>";