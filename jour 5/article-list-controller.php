<?php

$articles = [
    [
        "id" => 0,
        "title" => "PHP 7.3.0 alpha 2 Released",
        "date" => "21 Jun 2018",
        "content"=> "The PHP team is glad to announce the release of the second PHP 7.3.0 version, PHP 7.3.0 Alpha 2. The rough outline of the PHP 7.3 release cycle is specified in the PHP Wiki.For source downloads of PHP 7.3.0 Alpha 2 please visit the download page. Windows sources and binaries can be found on windows.php.net/qa/.Please carefully test this version and report any issues found in the bug reporting system.THIS IS A DEVELOPMENT PREVIEW - DO NOT USE IT IN PRODUCTION!For more information on the new features and other changes, you can read the NEWS file, or the UPGRADING file for a complete list of upgrading notes. These files can also be found in the release archive.The next release would be Alpha 3, planned for July 5.The signatures for the release can be found in the manifest or on the QA site.Thank you for helping us make PHP better."
    ],
    [
        "id" => 1,
        "title" => "PHP 5.6.34 Released",
        "date" => "01 Mar 2018",
        "content"=> "The PHP development team announces the immediate availability of PHP 5.6.34. This is a security release. One security bug was fixed in this release. All PHP 5.6 users are encouraged to upgrade to this version. For source downloads of PHP 5.6.34 please visit our downloads page, Windows source and binaries can be found on windows.php.net/download/. The list of changes is recorded in the ChangeLog."
    ],
    [
        "id" => 2,
        "title" => "PHP 7.2.3 Released",
        "date" => "01 Mar 2018",
        "content"=> "The PHP development team announces the immediate availability of PHP 7.2.3. This is a security release with also contains several minor bug fixes. All PHP 7.2 users are encouraged to upgrade to this version. For source downloads of PHP 7.2.3 please visit our downloads page, Windows source and binaries can be found on windows.php.net/download/. The list of changes is recorded in the ChangeLog."
    ],
];

include 'article-list-view.php';