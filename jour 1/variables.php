<?php



// une variable existe dans le contexte d'un programme
// les variables représentent de l'information stockée en RAM (mémoire)
    // elles ont un nom qui facilite la tâche du développeur pour accéder à un emplacement mémoire spécifique
    // la valeur 'stockée' dans la variable peut... varier.

// stocker l'heure (de début) dans une variable
// echo 'bonjour';
// echo 'le';
// echo 'monde';
// stocker l'heure (de fin) dans une variable
// afficher heure de fin - heure de debut



// echo "bonjour <nom>";
// echo "longueur du <nom>";
// echo "le <nom> à l'envers";

// Déclaration de variable
$my_variable; // NULL qui représente l'absence de valeur.
echo $my_variable;
var_dump($my_variable); // pour debugger le contenu des variables

$my_variable = 42; // affecte ou assigne 42 à la variable $my_variable. Première affectation = initialisation
$my_variable = 100; // affecte 100 à $my_variable

$my_other_variable = 'hello'; // déclaration et assignation sur la même ligne
var_dump($my_other_variable);

$my_other_variable = 10; // assignation d'une valeur d'un autre type => changement dynamique de type
var_dump($my_other_variable);


$my_variable = 0b101010;
var_dump($my_variable);

// color: #FF0000 <=> rgb(255,0,0)
// #FF0000













    // stocke de l'information 'dans un programme'
    // une variable c'est une sorte de boite
        // dans la boite il y a quoi ? une adresse mémoire




// dans un disque dur on met des fichiers ?

// variable est un objet qui peut prendre une valeur et qui peut changer de valeur.

// une variable c'est virtuel
// on lui donne un nom arbitraire

// à partir du moment ou l'exécution du programme est finie, cette mémoire est libérée
// un variable ça a un type ?

// espace de stockage - boite de rangement - emplacement dans la mémoire vive

// stocke des informations à un endroit pour y revenir plus tard

// on peut pas mettre autant de données dans une variable que dans un disque dur
// stocker l'information pour pouvoir la réutiliser ailleurs

